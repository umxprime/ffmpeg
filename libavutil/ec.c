/*
 * This file is part of FFmpeg.
 *
 * FFmpeg is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * FFmpeg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with FFmpeg; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include "config.h"
#include "ec.h"

AVECInfo *av_eci_create_side_data(AVFrame *f)
{
    AVBufferRef *buf = NULL;
    AVECInfo *eci = av_mallocz(sizeof(AVECInfo));

    if (!eci)
        return NULL;

    buf = av_buffer_create((uint8_t *)eci, sizeof(AVECInfo), NULL, NULL, 0);
    if (!buf) {
        av_freep(&eci);
        return NULL;
    }

    if (!av_frame_new_side_data_from_buf(f, AV_FRAME_DATA_EC_INFO, buf)) {
        av_buffer_unref(&buf);
        return NULL;
    }

    return eci;
}
