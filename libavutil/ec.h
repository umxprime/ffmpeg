/*
 * This file is part of FFmpeg.
 *
 * FFmpeg is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * FFmpeg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with FFmpeg; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef AVUTIL_EC_H
#define AVUTIL_EC_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "libavutil/mem.h"
#include "libavutil/frame.h"

/**
 * Error Concealment information helpful to a renderer or end user
 * attempting to filter or conceal video decoding errors and artifacts.
 */
typedef struct AVECInfo {
    /**
     * Integer estimating how many pixels of the video frame had decoding
     * errors.
     */
    uint64_t error;
    /**
     * Integer estimating how many pixels of the video frame decoded
     * without error.
     */
    uint64_t ok;
    /**
     * Integer estimating how many pixels of the video frame's reference
     * frames had decoding errors.
     */
    uint64_t ref_error;
    /**
     * Integer estimating how many pixels of the video frame's reference
     * frames decoded without error.
     */
    uint64_t ref_ok;
} AVECInfo;

static inline void av_eci_reset(AVECInfo *info)
{
    info->error = info->ok = info->ref_error = info->ref_ok = 0;
}

/**
 * Creates AVECInfo in a specified frame as side data.
 */
AVECInfo *av_eci_create_side_data(AVFrame *f);

#endif /* AVUTIL_EC_H */
