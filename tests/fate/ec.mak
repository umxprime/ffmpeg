FATE_EC-$(call ALLYES, ERROR_RESILIENCE H264_PARSER H264_DECODER) += fate-ec-sidedata-h264
fate-ec-sidedata-h264: SRC = $(TARGET_SAMPLES)/ec/basic.h264
fate-ec-sidedata-h264: CMD = run ffprobe$(PROGSSUF)$(EXESUF) -export_side_data +error_info -show_entries side_data -print_format default -bitexact -v 0 -i "$(SRC)"

FATE_SAMPLES_FFMPEG+=$(FATE_EC-yes)
fate-ec: $(FATE_EC-yes)
